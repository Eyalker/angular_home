// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Angular Material 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatCardModule} from '@angular/material/card';
import { MatExpansionModule} from '@angular/material/expansion';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatSelectModule} from '@angular/material';
import { MatInputModule} from '@angular/material';


//AngularFire
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

// Routing
import { RouterModule, Routes } from '@angular/router';

// Components
import { AppComponent } from './app.component';
import { NavComponent } from './Components/nav/nav.component';
import { BooksComponent } from './Components/books/books.component';
import { AuthorsComponent } from './Components/authors/authors.component';
import { EditauthorComponent } from './Components/editauthor/editauthor.component';
import { PostsComponent } from './Components/posts/posts.component';
import { PostsService } from './posts.service';

const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'authors', component: AuthorsComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'authors/:authorname/:id', component: AuthorsComponent },
  { path: 'editauthors/:authorname/:id' ,  component: EditauthorComponent},
  
  { path: "",
    redirectTo: '/',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent,
    AuthorsComponent,
    EditauthorComponent,
    PostsComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    AngularFireModule.initializeApp(environment.firebaseconfig, 'ClassBooks'),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    MatCardModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule,


  ],
  providers: [PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
