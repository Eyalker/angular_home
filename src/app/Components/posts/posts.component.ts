
import { Component, OnInit } from '@angular/core';
import { PostsService } from 'src/app/posts.service';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;
  posts$

  constructor(private postservice:PostsService) { }

  ngOnInit() {
    this.posts$ = this.postservice.getPost();
  }

}